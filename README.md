## List Comprehensions & Lambda

Una compresión de lista `list comprehension` tiene su equivalente en una función lambda.

Dada la siguiente compresión de lista:

```python
sum_exp = [x+2**2 for x in range(10)]

```

Define la función `list_to_lambda` que use el equivalente de esta `list comprehension` y se obtenga el mismo resultado.

```python
"""Driver Code"""

sum_exp = [x+2**2 for x in range(10)]
print(sum_exp == [4, 5, 6, 7, 8, 9, 10, 11, 12, 13])
#True

sum_exp = list_to_lambda(range(10))
print(sum_exp == [4, 5, 6, 7, 8, 9, 10, 11, 12, 13])
#True

print(list_to_lambda(range(6)) == [4, 5, 6, 7, 8, 9])

print(list_to_lambda(range(12)) == [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])

print(list_to_lambda(range(0)) == "Invalid Operation")

```



> Es importante investigar acerca de funciones lambda.